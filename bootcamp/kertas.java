package tes.bootcamp;

import java.util.Scanner;

public class kertas {
//	Ada berapa lembar kertas A6 yang bisa disatukan untuk membuat selembar kertas berukuran Ax ?
	public static void main(String[] args) {
		Scanner baca = new Scanner(System.in);

		int A6 = 1;
		int A1;
		int A2;
		int A3;
		int A4;
		int A5;
		
		System.out.print("Pilih ukuran kertas: ");
		String input = baca.nextLine();
		switch (input) {
		case "A1":
			A1 = A6 * 32;
			System.out.println(A1+" lembar");
			break;
		case "A2":
			A2 = A6 * 16;
			System.out.println(A2+" lembar");
			break;
		case "A3":
			A3 = A6 * 8;
			System.out.println(A3+" lembar");
			break;
		case "A4":
			A4 = A6 * 4;
			System.out.println(A4+" lembar");
			break;
		case "A5":
			A5 = A6 * 2;
			System.out.println(A5+" lembar");
			break;

		default:
			break;
		}
		baca.close();
	}

}
