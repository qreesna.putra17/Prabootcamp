package tes.bootcamp;

import java.util.Scanner;
//Bila x = 7
//1    3    5    7    9    11    13
//13    11    9    7    5    3    1
//
//Bila x = 11
//1    3    5    7    9    11    13    15    17    19    21
//21    19    17    15    13    11    9    7    5    3    1

public class deretReverse {

	public static void main(String[] args) {
		Scanner baca = new Scanner(System.in);
		System.out.println("Input x: ");
		int x= baca.nextInt();

		int n = 1;
		int []nilai = new int[x];
		int asc;
		for (int i = 0; i < nilai.length; i++) {
			nilai[i] = n; 
			System.out.print(nilai[i]+ " ");
			n=n+2;
		}
		
		for (int i = 1; i < nilai.length; i++) {
			for (int j = 0; j < nilai.length-1; j++) {
				if(nilai[j]<nilai[j+1]) {
					asc = nilai[j];
					nilai[j] = nilai[j+1];
					nilai[j+1] = asc;
				}
			}
		}
		
		System.out.println();
		
		for (int i = 0; i < nilai.length; i++) {
			System.out.print(nilai[i]+" ");
		}
		
		baca.close();
	}

}
