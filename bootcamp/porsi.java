package tes.bootcamp;

import java.util.Scanner;

public class porsi {
//	Tiap 1 orang dewasa laki-laki memakan 2 piring nasi goreng, 1 orang dewasa perempuan memakan 1 piring mie goreng, 2 orang remaja memakan
//	2 mangkok mie ayam, 1 orang anak-anak memakan 1/2 piring nasi goreng, 1 orang balita memakan 1 mangkok kecil bubur sehat. 
//	Berapa total porsi makanan yang dimakan ?
//
//			Constraints :
//			    -    Jika total yang sedang makan jumlahnya ganjil lebih dari 5 orang maka tiap orang dewasa perempuan tambah 1 piring nasi goreng   
//			    -    Inputan bisa saja acak (misalnya: laki-laki dewasa 3, balita 2, laki-laki dewasa 2, balita 2, perempuan dewasa 3)
//
//			Contoh :
//			Input : Laki-laki dewasa = 3 orang; Perempuan dewasa = 1 orang; Balita = 1 orang; Laki-laki dewasa = 1 orang;
//			Output : 10 porsi
	public static void main(String[] args) {
		Scanner baca = new Scanner(System.in);
		
		int plaki = 2;
		int pperempuan = 1;
		int premaja = 1;
		double panak = 0.5;
		int pbalita = 1;
		
		System.out.print("Jumlah laki-laki dewasa: ");
		int laki = baca.nextInt();
		System.out.print("Jumlah perempuan dewasa: ");
		int perempuan = baca.nextInt();
		System.out.print("Jumlah remaja: ");
		int remaja = baca.nextInt();
		System.out.print("Jumlah anak-anak: ");
		int anak = baca.nextInt();
		System.out.print("Jumlah balita: ");
		int balita = baca.nextInt();

		
		int jmlOrang = laki + perempuan + remaja + anak + balita;
		
		if((jmlOrang % 2 == 1) && jmlOrang > 5) {
			pperempuan = pperempuan + 1;
		}
		
		double total = (plaki*laki) + (pperempuan*perempuan) + (premaja*remaja) + (panak*anak) + (balita*pbalita);
		System.out.println("Total pesanan: "+total+ " porsi");
		
		baca.close();
	}

}
