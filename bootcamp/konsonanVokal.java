package tes.bootcamp;

import java.util.Scanner;

public class konsonanVokal {
//	Problem : urutkan huruf/karakter dari gabungan beberapa kata atau sebuah kalimat sesuai dengan abjad alfabet
//	Input : gabungan beberapa kata atau sebuah kalimat
//	Constraints :
//	    - mengandung huruf vokal dan konsonan
//	    - urut dan pisahkan huruf/karakter tersebut ke dalam 2 kelompok; kelompok vokal dan konsonan
//	    - pisahkan antara vokal dan konsonan
//	    - diproses sebagai huruf kecil (spasi diabaikan)
//	Output : 2 kelompok huruf; vokal dan konsonan
//
//	Example
//	input n : Sample Case
//	output : Huruf vokal : aaee
//	Huruf konsonan : clmpss
//
//	input n :  Next Case
//	output : Huruf vokal : aee
//	Huruf konsonan : cnstx
	public static void main(String[] args) {
		Scanner baca = new Scanner(System.in);
		System.out.println("Input kata n: ");
		String kata = baca.nextLine();
		String vokal ="";
		String kons = "";
 				
		String []Ckata = kata.split("");
		for (int i = 0; i < Ckata.length; i++) {
			if(Ckata[i].equalsIgnoreCase("a") || Ckata[i].equalsIgnoreCase("i") || Ckata[i].equalsIgnoreCase("u") || 
			Ckata[i].equalsIgnoreCase("e") || Ckata[i].equalsIgnoreCase("o")) {
				vokal = vokal + Ckata[i];
			} else {
				kons = kons + Ckata[i];
			}
		}

		System.out.println("Huruf vokal: "+vokal);
		System.out.println("Huruf konsonan: "+kons);
	baca.close();
	}

}
