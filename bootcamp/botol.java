package tes.bootcamp;

import java.util.Scanner;

public class botol {
//	Jika 1 botol = 2 gelas, 1 teko = 25 cangkir, 1 gelas = 2,5 cangkir.
//			Buatlah sistem konversi volume berdasarkan data di atas
//			Contoh :
//			    1 botol = ... cangkir ?
//			    1 botol = 5 cangkir
	public static void main(String[] args) {
		Scanner baca = new Scanner(System.in);
		
		int cangkir = 1;
		double gelas = 2.5 * cangkir;
		double botol = 2 * gelas;
		double teko = 25 * cangkir;
		
		System.out.print("Nama barang: ");
		String input = baca.nextLine();
		System.out.println("Jumlah barang: ");
		int jumlah = baca.nextInt();
		
		switch (input) {
		case "teko":
			teko = 25 * cangkir * jumlah;
			System.out.println(jumlah+ " "+input+" = "+teko+" cangkir");
			break;
		case "botol":
			botol = 2 * cangkir * jumlah;
			System.out.println(jumlah+ " "+input+" = "+botol+" gelas");
			botol = 5 * cangkir * jumlah;
			System.out.println(jumlah+ " "+input+" = "+botol+" cangkir");
			break;
		case "gelas":
			gelas = 2.5 * cangkir * jumlah;
			System.out.println(jumlah+ " "+input+" = "+gelas+" cangkir");
			break;
		default:
			break;
		}
		
		baca.close();
	}
}
