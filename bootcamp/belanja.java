package tes.bootcamp;

import java.util.Scanner;

public class belanja {
//	Devi memesan sebuah tas secara online, dan barang akan dikirimkan dalam waktu 7 hari kerja.
//	Dari info hari dan tanggal pemesanan Devi, serta info hari libur nasional, 
//	maka pada hari dan tanggal berapakah pesanan Devi akan sampai? Jika barang akan tiba di bulan berikutnya, 
//			tambahkan keterangan (tanggal x di bulan berikutnya)
////
//			Constraints:
//			    - Asumsikan ada 31 hari dalam 1 bulan.
//			    - Jika di bulan itu tidak ada hari libur nasional, tulis 0.
//			    - Di bulan berikutnya tidak ada hari libur nasional.
//
//			Contoh:   
//			Input:
//			    - Tanggal dan Hari pemesanan: 25 Sabtu
//			    - Hari libur nasional: 26, 29
//			Output:    tanggal 5 di bulan berikutnya
	public static void main(String[] args) {
		Scanner baca = new Scanner(System.in);
		System.out.print("Masukkan tanggal pemesanan: ");
		int pesan = baca.nextInt();
		int bulan = 0;
		String ket = "";
		
		System.out.print("Input jumlah hari libur nasional: ");
		int input = baca.nextInt();
		int []libur = new int[input];

		for (int i = 0; i < libur.length; i++) {
			System.out.print("Libur nasional ke-"+(i+1)+": Tanggal ");
			libur[i] = baca.nextInt();
		}

		int sampai = 0;
		int jum = 0;

		sampai = pesan + 7;		
		for (int i = 0; i < libur.length; i++) {
			if(libur[i] > pesan && libur[i] < sampai) {
				jum++;
			}
		}
		sampai = sampai + jum;
		if(sampai>31) {
			sampai = sampai - 31;
			bulan++;
		} else {
			sampai = pesan + 7;
		}
		
		if(bulan == 1) {
			ket = "bulan depan";
		}
	
		System.out.println("Tanggal sampai: Tanggal "+sampai+"");
		System.out.println(ket);
	}

}
