package tes.bootcamp;

import java.util.Scanner;

public class primaFibonacciJum {
//	Buatlah deret angka bilangan prima, bilangan fibonacci, dan deret angka yang terbentuk dari penjumlahan deret bilangan prima dan bilangan fibonacci pada masing-masing indexnya.
//	Input : Panjang array/panjang deret
//	Output :
//	    - deret prima
//	    - deret fibonacci
//	    - deret hasil penjumlahan
//
//	Contoh :
//	Input : 7
//	Output :
//	    - 2, 3, 5, 7, 11, 13, 17
//	    - 0, 1, 1, 2, 3, 5, 8
//	    - 2, 4, 6, 9, 14, 18, 25
	public static void main(String[] args) {
		Scanner baca = new Scanner(System.in);
		System.out.print("Masukan Jumlah Deret Fibonacci ");
        int input = baca.nextInt();
        long fibo[] = new long[input];
         
        fibo[0] = 0;
        fibo[1] = 1;
         
        for(int i = 2; i < input; i++) {
            fibo[i] = fibo[i-1] + fibo[i-2];
        }
         
        for (int i = 0; i < input; i++) {
            System.out.print(fibo[i] +  " ");
        }
        
        baca.close();
	}

}
