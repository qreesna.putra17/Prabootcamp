package tes.bootcamp;

//Buatlah fungsi untuk kalkulasi tarif parkir berdasarkan ketentuan berikut
//Ketentuan tarif:
//1. Delapan jam pertama: 1.000/jam
//2. Lebih dari 8  jam s.d. 24 jam: 8.000 flat
//3. Lebih dari 24 jam: 15.000/24 jam dan selebihnya mengikuti ketentuan pertama dan kedua
//
//Input:
//    - Tanggal dan jam masuk
//    - Tanggal dan jam keluar
//Output:    Besarnya tarif parkir
//
//Contoh:
//    - Masuk: 28 Januari 2020 07:30:34
//    - Keluar: 28 Januari 2020 20:03:35
//Output:    8000
//
//Penjelasan:
//Lamanya parkir adalah 12 jam 33 menit 1 detik, sehingga perhitungan tarif parkir dibulatkan menjadi 13 jam.
//Mengacu pada ketentuan kedua, maka yang harus dibayarkan adalah 8000 rupiah

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class parkir {
	
	public static void main(String[] args) throws ParseException {
		String tglmasuk = "28/01/2020 07:30:34";
		String tglkeluar = "29/01/2020 07:35:35";
		
		//buat format
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		//convert
		Date xtglmasuk = (Date)format.parse(tglmasuk);
		Date xtglkeluar = (Date)format.parse(tglkeluar);
		
		//milisecond
		long bedawaktu = xtglkeluar.getTime() - xtglmasuk.getTime();
		
		//milisecond to day, hours, minutes
		long bedahari = TimeUnit.MILLISECONDS.toDays(bedawaktu);
		long bedajam = TimeUnit.MILLISECONDS.toHours(bedawaktu);
		long bedamenit = TimeUnit.MILLISECONDS.toMinutes(bedawaktu);
		int tarifA = 0;
		int tarifB = 0 ;
		
		int xbeda = (int) bedamenit - ((int)bedajam*60); 
		if(xbeda > 0) {
			bedajam = bedajam + 1;
		}
		
		if(bedajam > 8 && bedajam <= 24) {
			tarifA = 8000;
		} else if(bedajam > 24) {
			bedajam = bedajam - 24;
			tarifA = (int)bedahari * 15000;
		} 
		
		if(bedajam <= 8) {
			tarifB = 1000 * (int)bedajam;
		}
		
		int total = tarifA + tarifB;
		
		System.out.println(total);
	}

}
